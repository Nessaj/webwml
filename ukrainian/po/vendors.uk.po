# translation of vendors.po to Ukrainian
# Eugeniy Meshcheryakov <eugen@debian.org>, 2005, 2006.
# Volodymyr Bodenchuk <Bodenchuk@bigmir.net>, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: vendors\n"
"PO-Revision-Date: 2017-11-17 01:12+0200\n"
"Last-Translator: Volodymyr Bodenchuk <Bodenchuk@bigmir.net>\n"
"Language-Team: українська <Ukrainian <ukrainian>>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Gtranslator 2.91.7\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n "
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: ../../english/CD/vendors/vendors.CD.def:14
msgid "Vendor"
msgstr "Постачальник"

#: ../../english/CD/vendors/vendors.CD.def:15
msgid "Allows Contributions"
msgstr "Дозволяє внески"

#: ../../english/CD/vendors/vendors.CD.def:16
msgid "CD/DVD/BD/USB"
msgstr "CD/DVD/BD/USB"

#: ../../english/CD/vendors/vendors.CD.def:17
msgid "Architectures"
msgstr "Архітектури"

#: ../../english/CD/vendors/vendors.CD.def:18
msgid "Ship International"
msgstr "Міжнародна доставка"

#: ../../english/CD/vendors/vendors.CD.def:19
msgid "Contact"
msgstr "Контакт"

#. ###################
#. Vendor home page link, Debian page link
#: ../../english/CD/vendors/vendors.CD.def:44
msgid "Vendor Home"
msgstr "Сторінка постачальника"

#: ../../english/CD/vendors/vendors.CD.def:70
msgid "page"
msgstr "сторінка"

#: ../../english/CD/vendors/vendors.CD.def:71
msgid "email"
msgstr "електронна пошта"

#: ../../english/CD/vendors/vendors.CD.def:114
msgid "within Europe"
msgstr "в межах Європи"

#: ../../english/CD/vendors/vendors.CD.def:118
msgid "To some areas"
msgstr "В деякі райони"

#: ../../english/CD/vendors/vendors.CD.def:122
msgid "source"
msgstr "джерельні коди"

#: ../../english/CD/vendors/vendors.CD.def:126
msgid "and"
msgstr "та"

#~ msgid "Official CD"
#~ msgstr "Офіційні CD"

#~ msgid "Official DVD"
#~ msgstr "Офіційні DVD"

#~ msgid "Development Snapshot"
#~ msgstr "Знімок для розробиків"

#~ msgid "Vendor Release"
#~ msgstr "Випуск продавця"

#~ msgid "Multiple Distribution"
#~ msgstr "Декілька дистрибутивів"

#~ msgid "non-US included"
#~ msgstr "з non-US"

#~ msgid "non-free included"
#~ msgstr "з non-free"

#~ msgid "contrib included"
#~ msgstr "з contrib"

#~ msgid "vendor additions"
#~ msgstr "додатки продавця"

#~ msgid "Custom Release"
#~ msgstr "Власний випуск"

#~ msgid "reseller of $var"
#~ msgstr "реселлер $var"

#~ msgid "reseller"
#~ msgstr "реселлер"

#~ msgid "updated weekly"
#~ msgstr "щотижневі оновлення"

#~ msgid "updated twice weekly"
#~ msgstr "оновлення два рази на тиждень"

#~ msgid "updated monthly"
#~ msgstr "оновлення раз на місяць"

#~ msgid "Architectures:"
#~ msgstr "Архітектури:"

#~ msgid "DVD Type:"
#~ msgstr "Тип DVD:"

#~ msgid "CD Type:"
#~ msgstr "Тип CD:"

#~ msgid "email:"
#~ msgstr "E-mail:"

#~ msgid "Ship International:"
#~ msgstr "Міжнародна доставка:"

#~ msgid "Country:"
#~ msgstr "Країна:"

#~ msgid "Allows Contribution to Debian:"
#~ msgstr "Дозволяє внесок для Debian"

#~ msgid "URL for Debian Page:"
#~ msgstr "URL для сторінки Debian:"

#~ msgid "Vendor:"
#~ msgstr "Постачальник:"
