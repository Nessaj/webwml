#use wml::debian::template title="Устаревшая документация"
#include "$(ENGLISHDIR)/doc/manuals.defs"
#include "$(ENGLISHDIR)/doc/obsolete.defs"
#use wml::debian::translation-check translation="4792632f7a20682a368627c66663b57ff1b3fa8b" maintainer="Lev Lamberov"

<h1 id="historical">Исторические документы</h1>

<p>Приведённые ниже документы либо были написаны очень давно и не обновлялись, либо
были написаны для предыдущих версий Debian и
не были обновлены до текущих версий. Информация в этих документах
устарела, но она всё ещё может быть интересна.</p>

<p>Более нерелевантная документация и документация более не служащая никаким целям
удалена, но исходный код большинства устареших руководств
можно найти в группе
<a href="https://salsa.debian.org/ddp-team/attic">attic команды документации</a>.</p>


<h2 id="user">Документация для пользователей</h2>

<document "Документация по dselect для начинающих" "dselect">

<div class="centerblock">
<p>
  Этот файл содержит документацию по dselect для начинающих пользователей, которая призвана помочь
  в успешной установке Debian. В этой документации объясняется не
  всё, поэтому когда вы в первый раз встретитесь с dselect, обратитесь к интерактивному учебнику.
</p>
<doctable>
  <authors "Stéphane Bortzmeyer">
  <maintainer "(?)">
  <status>
  stalled: <a href="https://packages.debian.org/aptitude">aptitude</a> заменил
  dselect в качестве стандартного интерфейса управления пакетами в Debian
  </status>
  <availability>
  <inddpvcs name="dselect-beginner" formats="html txt pdf ps"
            langs="ca cs da de en es fr hr it ja pl pt ru sk" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

<document "Руководство пользователя" "users-guide">

<div class="centerblock">
<p>
Это <q>Руководство пользователя</q> является ничем иным как переформатированным <q>Руководством обучающегося пользователя</q>.
Содержание было изменено так, чтобы оно подходило для стандартной системы Debian.</p>

<p>Более 300 страниц с хорошим пособием о том, как начать использовать систему Debian;
раскрывает темы от <acronym lang="ru" title="Графического пользовательского интерфейса">GUI</acronym> до
оболочки командной строки.
</p>
<doctable>
  <authors "Progeny Linux Systems, Inc.">
  <maintainer "Osamu Aoki (&#38738;&#26408; &#20462;)">
  <status>
  Полезно в качестве пособия. Написано для выпуска woody,
  устарело.
  </status>
  <availability>
# langs="en" isn't redundant, it adds the needed language suffix to the link
  <inddpvcs name="users-guide" index="users-guide" langs="en" formats="html txt pdf" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

<document "Пособие по Debian" "tutorial">

<div class="centerblock">
<p>
Это руководство предназначено для новых пользователей Linux, оно призвано помочь познакомиться
с Linux, когда они уже установили его, или для новых пользователей Linux, использующих систему,
которую администрирует кто-то другой.
</p>
<doctable>
  <authors "Havoc Pennington, Oliver Elphick, Ole Tetlie, James Treacy,
  Craig Sawyer, Ivan E. Moore II">
  <editors "Havoc Pennington">
  <maintainer "(?)">
  <status>
  не поддерживается; неполно;
  вероятно, устарело и может быть заменено <a href="user-manuals#quick-reference">Справочником Debian</a>
  </status>
  <availability>
  не полно
  <inddpvcs name="debian-tutorial" vcsname="tutorial" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

<document "Debian GNU/Linux: Руководство по установке и использованию" "guide">

<div class="centerblock">
<p>
  Руководство, ориентированное на конечного пользователя.
</p>
<doctable>
  <authors "John Goerzen, Ossama Othman">
  <editors "John Goerzen">
  <status>
  готово (но оно предназначено для potato)
  </status>
  <availability>
  <inoldpackage "debian-guide">
  </availability>
</doctable>
</div>

<hr />

<document "Справочное руководство пользователя Debian" "userref">

<div class="centerblock">
<p>
  Это руководство предоставляет по меньшей мере обзор всего, что должен знать пользователь
  о своей системе Debian GNU/Linux (т.е., настройка X, то, как настроить
  сеть, как получить доступ к дискетам, и т.д.). Оно предназначено для устранения пробела
  между пособием по Debian и более подробным руководством и страницами info,
  которые прилагаются к каждому пакету.</p>

  <p>Оно предназначено для обучения тому, как комбинировать команды в
  соответствии с общим принципом Unix, который гласит, что <em>всегда существует более, чем один способ
  сделать это</em>.
</p>
<doctable>
  <authors "Ardo van Rangelrooij, Jason D. Waterman, Havoc Pennington,
      Oliver Elphick, Bruce Evry, Karl-Heinz Zimmer">
  <editors "Thalia L. Hooker, Oliver Elphick">
  <maintainer "(?)">
  <status>
  не поддерживается и весьма неполно;
  вероятно, устарело и может быть заменено <a href="user-manuals#quick-reference">Справочником Debian</a>
  </status>
  <availability>
  <inddpvcs name="user" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />


<document "Руководство системного администратора Debian" "system">

<div class="centerblock">
<p>
  Этот документ упомянут во введении к руководству о Политике Debian.
  Настоящий документ раскрывает все аспекты системного администрирования системы Debian.
</p>
<doctable>
  <authors "Tapio Lehtonen">
  <maintainer "(?)">
  <status>
  не поддерживается; неполно;
  вероятно, устарело и может быть заменено <a href="user-manuals#quick-reference">Справочником Debian</a>
  </status>
  <availability>
  не доступно
  <inddpvcs name="system-administrator" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

<document "Руководство сетевого администратора Debian" "network">

<div class="centerblock">
<p>
  Это руководство раскрывает все аспекты сетевого администрирования системы Debian.
</p>
<doctable>
  <authors "Ardo van Rangelrooij, Oliver Elphick, Duncan C. Thomson, Ivan E. Moore II">
  <maintainer "(?)">
  <status>
  не поддерживается; неполно;
  вероятно, устарело и может быть заменено <a href="user-manuals#quick-reference">Справочником Debian</a>
  </status>
  <availability>
  не доступно
  <inddpvcs name="network-administrator" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

# Add this to books, there's a revised edition (2nd) @ Amazon
<document "Поваренная книга Linux" "linuxcookbook">

<div class="centerblock">
<p>
  Практическое справочное руководство по системе Debian GNU/Linux, которое показывает
  (в более, чем 1500 <q>рецептах</q>), как использовать её в ежедневной деятельности &mdash; от
  работы с текстом, изображениями и звуком до решения проблем с производительностью и
  сетью. Подобно описываемому ПО эта книга выпущена под свободной лицензией,
  а её исходные данные свободно доступны.
</p>
<doctable>
  <authors "Michael Stutz">
  <status>
  опубликовано; написано для woody, устарело
  </status>
  <availability>
  <inoldpackage "linuxcookbook">
  <p><a href="http://dsl.org/cookbook/">от автора</a>
  </availability>
</doctable>
</div>

<hr />

<document "Руководство по APT" "apt-howto">

<div class="centerblock">
<p>
  Это руководство претендует на то, чтобы быть быстрым, но полным источником информации
  о системе APT и ее возможностях. Руководство содержит много информации
  об основных способах использования APT и множество примеров.
</p>
<doctable>
  <authors "Gustavo Noronha Silva">
  <maintainer "Gustavo Noronha Silva">
  <status>
   устарел с 2009 года
  </status>
  <availability>
  <inoldpackage "apt-howto">
  <inddpvcs name="apt-howto" langs="en ca cs de es el fr it ja pl pt-br ru uk tr zh-tw zh-cn"
	    formats="html txt pdf ps" naming="locale" vcstype="attic"/>
  </availability>
</doctable>
</div>

<h2 id="devel">Документация разработчика</h2>

<document "Введение: создание пакета Debian" "makeadeb">

<div class="centerblock">
<p>
  Введение о том, как создавать <code>.deb</code>, используя
  <strong>debmake</strong>.
</p>
<doctable>
  <authors "Jaldhar H. Vyas">
  <status>
  не поддерживается, устарело и может быть заменено <a href="devel-manuals#maint-guide">Руководством нового сопровождающего</a>
  </status>
  <availability>
  <a href="https://people.debian.org/~jaldhar/">HTML online</a>
  </availability>
</doctable>
</div>

<hr />

<document "Руководство программиста Debian" "programmers">

<div class="centerblock">
<p>
  Помогает новым разработчикам создавать пакеты для системы Debian GNU/Linux.
</p>
<doctable>
  <authors "Igor Grobman">
  <status>
  устарело и может быть заменено <a href="devel-manuals#maint-guide">Руководством нового сопровождающего</a>
  </status>
  <availability>
  <inddpvcs name="programmer" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

<document "Руководство по созданию пакетов Debian" "packman">

<div class="centerblock">
<p>
  Это руководство описывает технические аспекты создания двоичных пакетов Debian
  и пакетов с исходным кодом. Оно также содержит документацию по интерфейсу между dselect
  и его сценариями методов доступа. Оно не касается требований Политики Проекта Debian,
  кроме того, предполагается знакомство с работой dpkg
  с точки зрения системного администратора.

<doctable>
  <authors "Ian Jackson, Klee Dienes, David A. Morris, Christian Schwarz">
  <status>
  Части, которые de facto были Политикой, включены в
  <a href="devel-manuals#policy">debian-policy</a>.
  </status>
  <availability>
  <inoldpackage "packaging-manual">
  </availability>
</doctable>
</div>

<hr />

<document "Как производители ПО могут распространять свои продукты непосредственно в формате .deb" "swprod">

<div class="centerblock">
<p>
  Этот документ рассматривается, как отправная точка объяснения, каким
  образом производители программного обеспечения могут интегрировать свои
  продукты в Debian, какие возможны ситуации в зависимости от лицензий
  продуктов и решений производителей, и что можно сделать в каждом случае.
  Он не объясняет, как создавать пакеты, но ссылается на другие документы,
  где это описывается.

  <p>Вам следует прочесть его, если вы не очень хорошо представляете себе
  картину создания и распространения пакетов Debian и, возможно, включения
  их в дистрибутив Debian.

<doctable>
  <authors "Aaron Isotton">
  <maintainer "Aaron Isotton">
  <status>
  устарел
  </status>
  <availability>
  <inddpvcs-distribute-deb>
  </availability>
</doctable>
</div>

<hr />

<document "Введение в интернационализацию (i18n)" "i18n">

<div class="centerblock">
<p>
  Этот документ описывает основные идеи и методы локализации (l10n),
  интернационализации (i18n) и многоязычности (m17n) для программистов и
  сопровождающих пакетов.

  <p>Цель этого документа&nbsp;&mdash; создать больше пакетов, поддерживающих
  интернационализацию и сделать Debian более международным дистрибутивом.
  Приглашаются участники со всего мира, поскольку автор говорит по-японски
  и если не найдется других участников, этот документ будет описывать только
  японизацию.

<doctable>
  <authors "Tomohiro KUBOTA (&#20037;&#20445;&#30000;&#26234;&#24195;)">
  <maintainer "Tomohiro KUBOTA (&#20037;&#20445;&#30000;&#26234;&#24195;)">
  <status>
  разработка прекращена, устарел
  </status>
  <availability>
  пока не готово
  <inddpvcs-intro-i18n>
  </availability>
</doctable>
</div>

<hr />

<document "Руководство по Debian SGML/XML" "sgml-howto">

<div class="centerblock">
<p>
  Это руководство содержит практическую информацию об использовании SGML и XML
  в операционной системе Debian.

<doctable>
  <authors "Stephane Bortzmeyer">
  <maintainer "Stephane Bortzmeyer">
  <status>
  разработка прекращена, устарело
  </status>
  <availability>

# English only using index.html, so langs set.
  <inddpvcs name="sgml-howto"
            formats="html"
            srctype="SGML"
            vcstype="attic"
/>
  </availability>
</doctable>
</div>

<hr />

<document "Политика Debian XML/SGML" "xml-sgml-policy">

<div class="centerblock">
<p>
  Политика для пакетов Debian, предоставляющих и/или использующих
  ресурсов XML или SGML.

<doctable>
  <authors "Mark Johnson, Ardo van Rangelrooij, Adam Di Carlo">
  <status>
  мёртв
  </status>
  <availability>
  <inddpvcs-xml-sgml-policy>
  </availability>
</doctable>
</div>

<hr />

<document "Руководство по разметке DebianDoc-SGML" "markup">

<div class="centerblock">
<p>
  Документация для системы <strong>debiandoc-sgml</strong>, включающая
  наилучшие практические приемы и подсказки для сопровождающих. Будущие версии должны включать
  советы для облегчения ведения и построения документации в пакетах Debian,
  руководящие принципы для организации перевода документации и другую полезную
  информацию. Также см. <a href="https://bugs.debian.org/43718">ошибку #43718</a>.

<doctable>
  <authors "Ian Jackson, Ardo van Rangelrooij">
  <maintainer "Ardo van Rangelrooij">
  <status>
  готово
  </status>
  <availability>
  <inpackage "debiandoc-sgml-doc">
  <inddpvcs-debiandoc-sgml-doc>
  </availability>
</doctable>
</div>

<hr>

<h2 id="misc">Различная документация</h2>

<document "Руководство по репозиториям Debian" "repo">

<div class="centerblock">
<p>
  Этот документ объясняет как работают репозитории Debian, как их
  создавать и как корректно добавлять их в <tt>sources.list</tt>.
</p>
<doctable>
  <authors "Aaron Isotton">
  <maintainer "Aaron Isotton">
  <status>
  готово (?)
  </status>
  <availability>
  <inddpvcs name="repository-howto" index="repository-howto"
            formats="html" langs="en fr de uk ta" vcstype="attic">
  </availability>
</doctable>
</div>
