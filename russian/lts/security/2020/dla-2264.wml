#use wml::debian::translation-check translation="874b7a827053e059c3b08e991a12da214544f123" mindelta="1" maintainer="Lev Lamberov"
<define-tag description>обновление безопасности LTS</define-tag>
<define-tag moreinfo>

<p>В libVNC (Debian-пакет libvncserver), реализации сервера и клиента в соответствии
с протоколом VNC, было обнаружено несколько уязвимостей.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20839">CVE-2019-20839</a>

    <p>libvncclient/sockets.c в LibVNCServer содержит переполнение буфера, возникающее
    из-за длинных имён файлов сокетов.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14397">CVE-2020-14397</a>

    <p>libvncserver/rfbregion.c содержит разыменование NULL-указателя.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14399">CVE-2020-14399</a>

    <p>Обращение к байт-синхронизированным данным в libvncclient/rfbproto.c
    осуществляется через указатели uint32_t.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14400">CVE-2020-14400</a>

    <p>Обращение к байт-синхронизированным данным в libvncserver/translate.c
    осуществляется через указатели uint16_t.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14401">CVE-2020-14401</a>

    <p>libvncserver/scale.c содержит переполнение целых чисел в pixel_value.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14402">CVE-2020-14402</a>

    <p>libvncserver/corre.c позволяет обращаться за пределы выделенного буфера памяти с помощью кодировок.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14403">CVE-2020-14403</a>

    <p>libvncserver/hextile.c позволяет обращаться за пределы выделенного буфера памяти с помощью кодировок.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14404">CVE-2020-14404</a>

    <p>libvncserver/rre.c позволяет обращаться за пределы выделенного буфера памяти с помощью кодировок.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14405">CVE-2020-14405</a>

    <p>libvncclient/rfbproto.c не ограничивает размер TextChat.</p></li>

</ul>

<p>В Debian 8 <q>Jessie</q> эти проблемы были исправлены в версии
0.9.9+dfsg2-6.1+deb8u8.</p>

<p>Рекомендуется обновить пакеты libvncserver.</p>

<p>Дополнительную информацию о рекомендациях по безопасности Debian LTS,
о том, как применять эти обновления к вашей системе, а также ответы на часто
задаваемые вопросы можно найти по адресу: <a href="https://wiki.debian.org/LTS">\
https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2264.data"
