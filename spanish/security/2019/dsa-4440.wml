#use wml::debian::translation-check translation="45c7a748884ba819e8e43a8bafa9c7b0a2629b5a"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se encontraron múltiples vulnerabilidades en el servidor DNS BIND:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5743">CVE-2018-5743</a>

    <p>No se hacían cumplir correctamente los límites de conexión.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5745">CVE-2018-5745</a>

    <p>La funcionalidad "managed-keys" era vulnerable a denegación de servicio mediante
    el desencadenamiento de una aserción.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6465">CVE-2019-6465</a>

    <p>No se hacían cumplir correctamente las ACL para transferencias de zona en el caso de zonas
    cargables dinámicamente (DLZ, «dynamically loadable zones»).</p></li>

</ul>

<p>Para la distribución «estable» (stretch), estos problemas se han corregido en
la versión 1:9.10.3.dfsg.P4-12.3+deb9u5.</p>

<p>Le recomendamos que actualice los paquetes de bind9.</p>

<p>Para información detallada sobre el estado de seguridad de bind9, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/bind9">\
https://security-tracker.debian.org/tracker/bind9</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4440.data"
