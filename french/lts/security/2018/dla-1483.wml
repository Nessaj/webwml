#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10871">CVE-2018-10871</a>

<p>Par défaut, nsslapd-unhashed-pw-switch était réglé à <q>on</q>. Donc une
copie du mot de passe non chiffré était conservée dans les modificateurs et
était éventuellement enregistrée dans le journal de modifications et
retroChangeLog.</p>

<p>À moins d’être utilisé par quelque greffon, il ne requiert pas de conserver
des mots de passe non chiffrés. L’option nsslapd-unhashed-pw-switch est
désormais <q>off</q> par défaut.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10935">CVE-2018-10935</a>

<p>N’importe quel utilisateur authentifié faisant une recherche en utilisant
ldapsearch avec des contrôles étendus pour une sortie côté serveur, pourrait
mettre à bas le serveur LDAP lui-même.</p>

<p>La correction est de vérifier s’il est possible d’indexer la valeur
fournie. Sinon slapd_qsort renvoie une erreur
(LDAP_OPERATION_ERROR).</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.3.3.5-4+deb8u2.</p>
<p>Nous vous recommandons de mettre à jour vos paquets 389-ds-base.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1483.data"
# $Id: $
