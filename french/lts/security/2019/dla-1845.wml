#use wml::debian::translation-check translation="be4d5c2d0ee602a6a885c9e6b4af43e386e6651a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans DOSBox, un
émulateur pour exécuter de vieux programmes DOS.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7165">CVE-2019-7165</a>

<p>Une très longue ligne dans un fichier bat pourrait déborder du tampon
d’analyse. Cela pourrait être utilisé par un attaquant pour exécuter du code
arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12594">CVE-2019-12594</a>

<p>Des contrôles d’accès insuffisants dans DOSBox permettaient à des attaquants
d’accéder aux ressources sur le système hôte et d'exécuter du code arbitraire.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 0.74-4+deb8u1.</p>
<p>Nous vous recommandons de mettre à jour vos paquets un déni de servicebox.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1845.data"
# $Id: $
