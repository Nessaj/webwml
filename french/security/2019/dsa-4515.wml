#use wml::debian::translation-check translation="9f4968f1d9a97ff25da0334028d0e74ab7c96c32" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le moteur web
webkit2gtk :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8644">CVE-2019-8644</a>

<p>G. Geshev a découvert des problèmes de corruption de mémoire qui peuvent
conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8649">CVE-2019-8649</a>

<p>Sergei Glazunov a découvert un problème qui pourrait conduire à une
vulnérabilité de script intersite universel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8658">CVE-2019-8658</a>

<p>akayn a découvert un problème qui pourrait conduire à une vulnérabilité
de script intersite universel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8666">CVE-2019-8666</a>

<p>Zongming Wang et Zhe Jin ont découvert des problèmes de corruption de
mémoire qui peuvent conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8669">CVE-2019-8669</a>

<p>akayn a découvert des problèmes de corruption de mémoire qui peuvent
conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8671">CVE-2019-8671</a>

<p>Apple a découvert des problèmes de corruption de mémoire qui peuvent
conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8672">CVE-2019-8672</a>

<p>Samuel Gross a découvert des problèmes de corruption de mémoire qui
peuvent conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8673">CVE-2019-8673</a>

<p>Soyeon Park et Wen Xu ont découvert des problèmes de corruption de
mémoire qui peuvent conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8676">CVE-2019-8676</a>

<p>Soyeon Park et Wen Xu ont découvert des problèmes de corruption de
mémoire qui peuvent conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8677">CVE-2019-8677</a>

<p>Jihui Lu a découvert des problèmes de corruption de mémoire qui peuvent
conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8678">CVE-2019-8678</a>

<p>Un chercheur anonyme, Anthony Lai, Ken Wong, Jeonghoon Shin, Johnny Yu,
Chris Chan, Phil Mok, Alan Ho et Byron Wai ont découvert des problèmes de
corruption de mémoire qui peuvent conduire à l'exécution de code
arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8679">CVE-2019-8679</a>

<p>Jihui Lu a découvert des problèmes de corruption de mémoire qui peuvent
conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8680">CVE-2019-8680</a>

<p>Jihui Lu a découvert des problèmes de corruption de mémoire qui peuvent
conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8681">CVE-2019-8681</a>

<p>G. Geshev a découvert des problèmes de corruption de mémoire qui peuvent
conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8683">CVE-2019-8683</a>

<p>lokihardt a découvert des problèmes de corruption de mémoire qui peuvent
conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8684">CVE-2019-8684</a>

<p>lokihardt a découvert des problèmes de corruption de mémoire qui peuvent
conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8686">CVE-2019-8686</a>

<p>G. Geshev a découvert des problèmes de corruption de mémoire qui peuvent
conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8687">CVE-2019-8687</a>

<p>Apple a découvert des problèmes de corruption de mémoire qui peuvent
conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8688">CVE-2019-8688</a>

<p>Insu Yun a découvert des problèmes de corruption de mémoire qui peuvent
conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8689">CVE-2019-8689</a>

<p>lokihardt a découvert des problèmes de corruption de mémoire qui peuvent
conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8690">CVE-2019-8690</a>

<p>Sergei Glazunov a découvert un problème qui pourrait conduire à une
vulnérabilité de script intersite universel.</p></li>

</ul>

<p>Plus de détails sont disponibles dans l'annonce de sécurité
WSA-2019-0004 de WebKitGTK et WPE WebKit.</p>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 2.24.4-1~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets webkit2gtk.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de webkit2gtk, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4515.data"
# $Id: $
