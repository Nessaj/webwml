#
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2009-02-22 14:29+0300\n"
"Last-Translator: Aleksandr Charkov <alcharkov@gmail.com>\n"
"Language-Team: unknown\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/Bugs/pkgreport-opts.inc:17
msgid "in package"
msgstr "pakete"

#: ../../english/Bugs/pkgreport-opts.inc:20
#: ../../english/Bugs/pkgreport-opts.inc:60
#: ../../english/Bugs/pkgreport-opts.inc:94
msgid "tagged"
msgstr "su žyme"

#: ../../english/Bugs/pkgreport-opts.inc:23
#: ../../english/Bugs/pkgreport-opts.inc:63
#: ../../english/Bugs/pkgreport-opts.inc:97
msgid "with severity"
msgstr "su svarbumo lygiu"

#: ../../english/Bugs/pkgreport-opts.inc:26
msgid "in source package"
msgstr "šaltinio pakete"

#: ../../english/Bugs/pkgreport-opts.inc:29
msgid "in packages maintained by"
msgstr "pakete kurį išlaiko"

#: ../../english/Bugs/pkgreport-opts.inc:32
msgid "submitted by"
msgstr "išsiųstame"

#: ../../english/Bugs/pkgreport-opts.inc:35
msgid "owned by"
msgstr "priklausantis"

#: ../../english/Bugs/pkgreport-opts.inc:38
msgid "with status"
msgstr "su statusu"

#: ../../english/Bugs/pkgreport-opts.inc:41
msgid "with mail from"
msgstr "el. pašto adresu"

#: ../../english/Bugs/pkgreport-opts.inc:44
msgid "newest bugs"
msgstr "naujausios klaidos"

#: ../../english/Bugs/pkgreport-opts.inc:57
#: ../../english/Bugs/pkgreport-opts.inc:91
msgid "with subject containing"
msgstr "su tema turinčia"

#: ../../english/Bugs/pkgreport-opts.inc:66
#: ../../english/Bugs/pkgreport-opts.inc:100
msgid "with pending state"
msgstr "laukimo būsenoje"

#: ../../english/Bugs/pkgreport-opts.inc:69
#: ../../english/Bugs/pkgreport-opts.inc:103
msgid "with submitter containing"
msgstr "su išsiuntinėju, turinčiu"

#: ../../english/Bugs/pkgreport-opts.inc:72
#: ../../english/Bugs/pkgreport-opts.inc:106
msgid "with forwarded containing"
msgstr "su persiuntinėju, turinčiu "

#: ../../english/Bugs/pkgreport-opts.inc:75
#: ../../english/Bugs/pkgreport-opts.inc:109
msgid "with owner containing"
msgstr "su savininku, turinčiu"

#: ../../english/Bugs/pkgreport-opts.inc:78
#: ../../english/Bugs/pkgreport-opts.inc:112
msgid "with package"
msgstr "iš paketo"

#: ../../english/Bugs/pkgreport-opts.inc:122
msgid "normal"
msgstr "normal"

#: ../../english/Bugs/pkgreport-opts.inc:125
msgid "oldview"
msgstr "oldview"

#: ../../english/Bugs/pkgreport-opts.inc:128
msgid "raw"
msgstr "raw"

#: ../../english/Bugs/pkgreport-opts.inc:131
msgid "age"
msgstr "amžius"

#: ../../english/Bugs/pkgreport-opts.inc:137
msgid "Repeat Merged"
msgstr "Pakartotinai  sujungtas"

#: ../../english/Bugs/pkgreport-opts.inc:138
msgid "Reverse Bugs"
msgstr "Klaidos atvirkščia eile"

#: ../../english/Bugs/pkgreport-opts.inc:139
msgid "Reverse Pending"
msgstr "Laukiami atvirkščia eile"

#: ../../english/Bugs/pkgreport-opts.inc:140
msgid "Reverse Severity"
msgstr "Atvirkščia eile pagal svarbumą"

#: ../../english/Bugs/pkgreport-opts.inc:141
msgid "No Bugs which affect packages"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:143
msgid "None"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:144
msgid "testing"
msgstr "ttesting"

#: ../../english/Bugs/pkgreport-opts.inc:145
msgid "oldstable"
msgstr "oldstable"

#: ../../english/Bugs/pkgreport-opts.inc:146
msgid "stable"
msgstr "stable"

#: ../../english/Bugs/pkgreport-opts.inc:147
msgid "experimental"
msgstr "experimental"

#: ../../english/Bugs/pkgreport-opts.inc:148
msgid "unstable"
msgstr "unstable"

#: ../../english/Bugs/pkgreport-opts.inc:152
msgid "Unarchived"
msgstr "Nearchyvuotos"

#: ../../english/Bugs/pkgreport-opts.inc:155
msgid "Archived"
msgstr "Archyvuotos"

#: ../../english/Bugs/pkgreport-opts.inc:158
msgid "Archived and Unarchived"
msgstr "Archyvuotos ir nearchyvuotos"

#~ msgid "Exclude tag:"
#~ msgstr "Pašalinti žymę:"

#~ msgid "Include tag:"
#~ msgstr "įskaityti žymę :"

#~ msgid "lfs"
#~ msgstr "lfs"

#~ msgid "ipv6"
#~ msgstr "ipv6"

#~ msgid "wontfix"
#~ msgstr "nenori ištaisyti "

#~ msgid "upstream"
#~ msgstr "prieš srovę"

#~ msgid "unreproducible"
#~ msgstr "neproduktyvus"

#~ msgid "security"
#~ msgstr "saugumas"

#~ msgid "patch"
#~ msgstr "lopas"

#~ msgid "moreinfo"
#~ msgstr "daugiau informacijos"

#~ msgid "l10n"
#~ msgstr "l10n"

#~ msgid "help"
#~ msgstr "padėti"

#~ msgid "fixed-upstream"
#~ msgstr "Pataisytas prieš srovę"

#~ msgid "fixed-in-experimental"
#~ msgstr "pataisytas eksperimentinėje"

#~ msgid "d-i"
#~ msgstr "d-i"

#~ msgid "confirmed"
#~ msgstr "patvirtintas"

#~ msgid "sid"
#~ msgstr "sid"

#~ msgid "etch-ignore"
#~ msgstr "etch-ignore"

#~ msgid "etch"
#~ msgstr "etch"

#~ msgid "sarge-ignore"
#~ msgstr "sarge-ignore"

#~ msgid "woody"
#~ msgstr "woody"

#~ msgid "potato"
#~ msgstr "potato"

#~ msgid "Exclude severity:"
#~ msgstr "Pašalinti griežtumą:"

#~ msgid "Include severity:"
#~ msgstr "įskaityti griežtumą:"

#~ msgid "wishlist"
#~ msgstr "pageidavimo sąrašas "

#~ msgid "minor"
#~ msgstr "mažesnis"

#~ msgid "important"
#~ msgstr "svarbus"

#~ msgid "serious"
#~ msgstr "rimtas"

#~ msgid "grave"
#~ msgstr "rimtas"

#~ msgid "critical"
#~ msgstr "kritiškas"

#~ msgid "Exclude status:"
#~ msgstr "Pašalinti statusą:"

#~ msgid "Include status:"
#~ msgstr "įskaityti statusą"

#~ msgid "done"
#~ msgstr "padarytas"

#~ msgid "fixed"
#~ msgstr "fiksuotas"

#~ msgid "pending"
#~ msgstr "laukimas"

#~ msgid "forwarded"
#~ msgstr "išsiųstas"

#~ msgid "open"
#~ msgstr "atviras"

#~ msgid "bugs"
#~ msgstr "klaidos"

#~ msgid "Distribution:"
#~ msgstr "Distribucija:"

#~ msgid "Package version:"
#~ msgstr "Paketo versiją:"

#~ msgid "testing-proposed-updates"
#~ msgstr "testing-proposed-updates"

#~ msgid "proposed-updates"
#~ msgstr "proposed-updates"

#~ msgid "don't show statistics in the footer"
#~ msgstr "Nerodyti statistiką apatiniame kolontitule"

#~ msgid "don't show table of contents in the header"
#~ msgstr "Nerodyti lenteles turinį viršutiniame kolontitule"

#~ msgid "no ordering by status or severity"
#~ msgstr "netvarkyti pagal statusą arba griežtumą"

#~ msgid "display merged bugs only once"
#~ msgstr "rodyti sujungtos klaidos tik viena kartą"

#~ msgid "active bugs"
#~ msgstr "aktyvios klaidos"

#~ msgid "Flags:"
#~ msgstr "Vėliavos:"
