#use wml::debian::template title="Emberek: kik vagyunk, mit csinálunk"
#include "$(ENGLISHDIR)/releases/info"

# translators: some text is taken from /intro/about.wml

# <!-- translated Szabolcs Siebenhofer <the7up@gmail.com> -->
#use wml::debian::translation-check translation="bbb1b67054b9061c9420f1c5826b2fa85f05c738"

<h2>Fejlesztők és közreműködők</h2>
<p>A Debian <a href="$(DEVEL)/developers.loc">szerte a világon</a> több ezer, a saját idejét 
feláldozó önkéntes fejlesztő közreműködésével jött létre. Néhányan a fejlesztők közül már 
találkoztak személyesen. A kommunikáció elsődlegesen e-mail-en keresztül 
(a levelező listák megtalálhatóak a lists.debian.org oldalon) és IRC-n (#debian csatorna
az irc.debian.org-on) zajlik</p>

<p>A hivatalos Debian fejlesztők listája megtalálható a 
<a href="https://nm.debian.org/members">nm.debian.org</a> oldalon, ahol a tagságukat 
nyilvántartjuk. A közreműködők hosszabb listája megtalálható a 
<a href="https://contributors.debian.org">contributors.debian.org</a> oldalon.</p>

<p>A Debian Projekt gondosan <a href="organization">szervezett struktúrával</a> rendelkezik.
A Debian belső felépítésével kapcsolatos további inforációkért ne habozz felkeresni 
a <a href="$(DEVEL)/">fejlesztők sarkát</a>.</p>

<h3><a name="history">Hogy kezdődött ez az egész?</a></h3>

<p>A Debian-t 1993-ban Ian Murdock indította, mint egy olyan új disztribúció, ami szabadon 
készül, a Linux és a GNU szellemében. A Debian-t gondosan és lelkiismeretesen szerették 
volna összerakni és hasonló módon karbantartni és támogatni. Szabad szoftver fejlesztők
 egy kicsi és jól szerevett csoportjaként indult és folyamatosan nőve fejlődött
 a fejlesztők és felhasználók jól szervezett közösségévé. Itt megtalálod  
<a href="$(DOC)/manuals/project-history/">a részletes történetet.</a>.

<p>Mivel sokan kérdezték, a Debian így kell kiejteni: /&#712;de.bi.&#601;n/. A Debian 
létrehozójának, Ian Murdock-nak és feleségének, Debra-nak a nevéből jött létre.
  
<h2>A Debian-t támogató személyek és szervezetek</h2>

<p>Nagyon sok ember és szervezet része a Debian közösségnek:
<ul>
  <li><a href="https://db.debian.org/machines.cgi">Tárhely és hardver támogatók</a></li>
  <li><a href="../mirror/sponsors">Tükrözés támogatók</a></li>
  <li><a href="../partners/">Fejlesztői és szolhgáltatási partnerek</a></li>
  <li><a href="../consultants">Szaktanácsadók</a></li>
  <li><a href="../CD/vendors">Debian telepítő média értékesítők</a></li>
  <li><a href="distrib/pre-installed">Számítógép értékesítők, akik előre telepítik a Debian-t</a></li>
  <li><a href="../events/merchandise">Reklámárú értékesítők</a></li>
</ul>

<h2><a name="users">Ki használ Debian-t?</a></h2>

<p>Habár pontos statisztika nem létezik (mivel a Debian nem kéri a 
felhasználókat, hogy regisztráljanak), elég erős bizonyítékok vannak, 
hogy a Debian széles körben használt kis és nagy szervezetek és több 
ezer személy által. Lásd a <a href="../users/">Ki használ Debian-t?</a>
 oldalunkat a nagy horderejű cégek listájáért, akik röviden leírták,
hogy hogyan és miért használnak Debian-t.
