#use wml::debian::template title="Introduction to Debian" MAINPAGE="true" 
#use wml::debian::recent_list

<a id=community></a>
<h2>Debian is a Community of People</h2>
<p>Thousands of volunteers around the world work together prioritizing Free
  Software and the needs of users.</p>

<ul>
  <li>
    <a href="people">People:</a>
    Who we are, what we do
  </li>
  <li>
    <a href="philosophy">Philosophy:</a>
    Why we do it and how we do it
  </li>
  <li>
    <a href="../devel/join/">Get involved:</a>
    You can be part of this!
  </li>
  <li>
    <a href="help">How can you help Debian?</a>
  </li>
  <li>
    <a href="../social_contract">Social Contract:</a>
    Our moral agenda
  </li>
  <li>
    <a href="diversity">Diversity Statement</a>
  </li>
  <li>
    <a href="../code_of_conduct">Code of Conduct</a>
  </li>
  <li>
    <a href="../partners/">Partners:</a>
    Companies and organizations that provide ongoing assistance to the Debian
    project
  </li>
  <li>
    <a href="../donations">Donations</a>
  </li>
  <li>
    <a href="../legal/">Legal Info </a>
  </li>
  <li>
    <a href="../legal/privacy">Data Privacy</a>
  </li>
  <li>
    <a href="../contact">Contact Us</a>
  </li>
</ul>

<hr>

<a id=software></a>
<h2>Debian is a Free Operating System</h2>
<p>We start with Linux and add many thousands of applications to meet the
  needs of its users.</p>

<ul>
  <li>
    <a href="../distrib">Download:</a>
    More variants of Debian images
  </li>
  <li>
  <a href="why_debian">Why Debian</a>
  </li>
  <li>
    <a href="../support">Support:</a>
    Getting help
  </li>
  <li>
    <a href="../security">Security:</a>
    Last update <br>
    <:{ $MYLIST = get_recent_list('security/1m', '1', '$(ENGLISHDIR)', 'bydate', '(2000\d+\w+|dsa-\d+)');
        @MYLIST = split(/\n/, $MYLIST);
        $MYLIST[0] =~ s#security#../security#;
        print $MYLIST[0]; }:>
  </li>
  <li>
    <a href="../distrib/packages"> Software packages:</a>
    Search and browse the long list of our software
  </li>
  <li>
    <a href="../doc"> Documentation</a>
  </li>
  <li>
    <a href="https://wiki.debian.org"> Debian wiki</a>
  </li>
  <li>
    <a href="../Bugs"> Bug reports</a>
  </li>
  <li>
    <a href="https://lists.debian.org/">
    Mailing Lists</a>
  </li>
  <li>
    <a href="../blends"> Pure Blends:</a>
    Metapackages for specific needs
  </li>
  <li>
    <a href="../devel"> Developers' Corner:</a>
    Information primarily of interest to Debian developers
  </li>
  <li>
    <a href="../ports"> Ports/Architectures:</a>
    CPU architectures we support
  </li>
  <li>
    <a href="search">Information on how to use the Debian search engine</a>.
  </li>
  <li>
    <a href="cn">Information on pages available in multiple languages</a>.
  </li>
</ul>
