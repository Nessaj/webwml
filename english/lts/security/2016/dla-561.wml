<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in uClibc, an
implementation of the standard C library that is much smaller than
glibc, which makes it useful for embedded systems.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2224">CVE-2016-2224</a>

    <p>Fix possible denial of service via a specially crafted DNS reply
    that could cause an infinite loop.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2225">CVE-2016-2225</a>

    <p>Fix possible denial of service via specially crafted packet that
    will make the parser in libc/inet/resolv.c terminate early.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6264">CVE-2016-6264</a>

    <p>It was found that <q>BLT</q> instruction in libc/string/arm/memset.S
    checks for signed values. If the parameter of memset is negative,
    then value added to the PC will be large. An attacker that controls
    the length parameter of memset can also control the value of PC
    register.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.9.32-1+deb7u1.</p>

<p>We recommend that you upgrade your uclibc packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-561.data"
# $Id: $
