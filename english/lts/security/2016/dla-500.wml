<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Bob Friesenhahn from the GraphicsMagick project discovered a command
injection vulnerability in ImageMagick, a program suite for image
manipulation. An attacker with control on input image or the input
filename can execute arbitrary commands with the privileges of the user
running the application.</p>

<p>This update removes the possibility of using pipe (|) in filenames to
interact with imagemagick.</p>

<p>It is important that you upgrade the libmagickcore5 and not just
the imagemagick package. Applications using libmagickcore5 might
also be affected and need to be restarted after the upgrade.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
8:6.7.7.10-5+deb7u6.</p>

<p>We recommend that you upgrade your imagemagick packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-500.data"
# $Id: $
